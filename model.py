import requests
import pandas as pd

class GithubModel():
  def __init__(self):
    self.url = 'https://jobs.github.com/positions.json'

  def busca_json(self, termo, pagina):
    resposta = self.chamar(termo, pagina)
    return resposta.json()

  def busca_csv(self, termo, pagina):
    resposta = self.chamar(termo, pagina)
    return self.transforma_em_json(resposta.content)

  def chamar(self, termo, pagina):    
    return requests.get(f'{self.url}?pagina={pagina}&pesquisa={termo}') 

  def transforma_em_json(self,json):
    f = pd.read_json(json)
    return f.to_csv()